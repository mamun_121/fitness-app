import React from "react";
import { View } from "react-native";
import {
    FontAwesome,
    MaterialIcons,
    MaterialCommunityIcons
} from "@expo/vector-icons";
import { white,red, pink, orange, lightPurp } from "./colors";

export function getMetricMetaInfo(metric) {
    const info = {
        run: {
            displayName: "Run",
            max: 50,
            unit: "miles",
            step: 1,
            type: "steppers",
            getIcon() {
                return (
                    <View>
                        <MaterialIcons name="directions-run" color={red} size={35} />
                    </View>
                );
            }
        },
        bike: {
            displayName: "Bike",
            max: 100,
            unit: "miles",
            step: 1,
            type: "steppers",
            getIcon() {
                return (
                    <View>
                        <MaterialCommunityIcons name="bike" color={'black'} size={32} />
                    </View>
                );
            }
        },
        swim: {
            displayName: "Swim",
            max: 9900,
            unit: "meters",
            step: 100,
            type: "steppers",
            getIcon() {
                return (
                    <View>
                        <MaterialCommunityIcons name="swim" color={pink} size={35} />
                    </View>
                );
            }
        },
        sleep: {
            displayName: "Sleep",
            max: 24,
            unit: "hours",
            step: 1,
            type: "slider",
            getIcon() {
                return (
                    <View>
                        <FontAwesome name="bed" color={orange} size={30} />
                    </View>
                );
            }
        },
        eat: {
            displayName: "Eat",
            max: 10,
            unit: "rating",
            step: 1,
            type: "slider",
            getIcon() {
                return (
                    <View>
                        <MaterialCommunityIcons name="food" color={lightPurp} size={35} />
                    </View>
                );
            }
        }
    };

    return typeof metric === "undefined" ? info : info[metric];
}

export function timeToString (time = Date.now()) {
    const date = new Date(time)
    const todayUTC = new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate()))
    return todayUTC.toISOString().split('T')[0]
}


