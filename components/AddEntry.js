import React, { Component } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import {getMetricMetaInfo, timeToString} from '../utils/helpers';
import FitSlider from '../components/FitSlider';
import FitSteppers from '../components/FitSteppers';
import DateHeader from '../components/DateHeader';
import { Ionicons } from '@expo/vector-icons'
import TextButton from '../components/TextButton';

function SubmitBtn ({ onPress }) {
    return (
        <TouchableOpacity
            onPress={onPress}>
            <Text>SUBMIT</Text>
        </TouchableOpacity>
    )
}
export default class AddEntry extends Component {
    state = {
        run: 0,
        bike: 0,
        swim: 0,
        sleep: 0,
        eat: 0,
    }
    // increment method for run, bike, swim object
    increment = (metric) => {
        // getting max and step from  getMetricMetaInfo with specific object run, bike, swim
        const {max, step}  = getMetricMetaInfo(metric);

        this.setState((state) => {
            // state[metric] --> run[0],
            const count = state[metric] + step;

            return {
                ...state,
                // does not exceed max value
                [metric]: count > max ? max : count
            }
        })
    };

    // decrement method for run, bike, swim object
    decrement = (metric) => {
        // getting step from  getMetricMetaInfo with specific object run, bike, swim
        const { step}  = getMetricMetaInfo(metric);

        this.setState((state) => {
            // state[metric] --> run[0],
            const count = state[metric] - step;

            return {
                ...state,
                // does not go below 0
                [metric]: count < 0 ? 0 : count
            }
        })
    };
    // slide method sleep and eat with specific metric and new value,
    slide = (metric, value) => {
        this.setState(()=> ({
            [metric] : value
        }))
    }

    submit = () => {
        const key = timeToString()
        const entry = this.state

        // Update Redux

        this.setState(() => ({
            run: 0,
            bike: 0,
            swim: 0,
            sleep: 0,
            eat: 0
        }))

        // Navigate to home

        // Save to "DB"

        // Clear local notification
    }

    reset = () => {
        const key = timeToString()

        // Update Redux

        // Route to Home

        // Update "DB"
    }
    render() {
        const metaInfo = getMetricMetaInfo();

        if (this.props.alreadyLogged) {
            return(
                <View>
                    <Ionicons
                        name='ios-happy'
                        size={100}
                    />
                    <Text>You already logged your information for today.</Text>

                    <TextButton onPress={this.reset}>
                        Reset
                    </TextButton>
                </View>
            );


        }
       return(
           <View>
               <DateHeader date={(new Date()).toLocaleDateString()} />
               {Object.keys(metaInfo).map((key)=>{
                   // return an array instead of object
                   const {getIcon, type, ...rest} = metaInfo[key]; // return getMetricMetaInfo with specific object
                   const value = this.state[key];
                   return(
                     <View key={key}>
                         {getIcon()}
                         {type === 'slider'
                         ? <FitSlider
                                 value={value}
                                 onChange={(value)=> this.slide(key, value)}
                                 {...rest}
                             />
                         : <FitSteppers
                                 value={value}
                                 onIncrement = {()=> this.increment(key)}
                                 ondecrement = {()=> this.decrement(key)}
                                 {...rest}
                             />
                         }
                     </View>
                   );
               })}

               <SubmitBtn onPress={this.submit} />
           </View>
       );
    }
}